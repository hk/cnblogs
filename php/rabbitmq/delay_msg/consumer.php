<?php
include '../conn.php';
$params = array(
    'exchangeName' => 'test_cache_exchange',
    'queueName' => 'test_cache_queue',
    'routeKey' => 'test_cache_route',
);

$conn = new AMQPConnection($conn_args);
if (!$conn->connect()) die("Cannot connect to the broker!\n");

$channel = new AMQPChannel($conn);
if(!$channel->isConnected()) die("channel fail");



//创建交换机对象
$ex = new AMQPExchange($channel);
$ex->setFlags(AMQP_DURABLE);
$ex->setName($params['exchangeName']);
$ex->setType(AMQP_EX_TYPE_DIRECT);
$ex->declareExchange();

$queue=new AMQPQueue($channel);

$queue->setName($params['queueName']);
$queue->setFlags(AMQP_DURABLE);
$queue->setArguments([
    'x-dead-letter-exchange' => 'delay_exchange',
    'x-dead-letter-routing-key' => 'delay_route',
    'x-message-ttl' => 5000, //毫秒，如果队列test_cache_queue已经存在，除非删除，否者修改时间会报错
]);
$queue->declareQueue();


$queue->bind($params['exchangeName'],$params['routeKey']);




$msg='product_time:'.date("Y-m-d H:i:s",time());
$r=$ex->publish($msg,$params['routeKey'],AMQP_MANDATORY,['delivery_mode'=>2]);
if($r){
    echo "send _success".PHP_EOL;
}

/*
 参考
https://blog.csdn.net/u012119576/article/details/74677835

*/
















