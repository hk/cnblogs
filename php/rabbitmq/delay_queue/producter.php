<?php
include '../conn.php';
$params = array(
    'exchangeName' => 'test_cache_exchange',
    'queueName' => 'test_cache_queue',
    'routeKey' => 'test_cache_route',
);

$conn = new AMQPConnection($conn_args);
if (!$conn->connect()) {
    die("Cannot connect to the broker!\n");
}

$channel = new AMQPChannel($conn);
if(!$channel->isConnected()) die("channel fail");



//创建交换机对象
$ex = new AMQPExchange($channel);
$ex->setFlags(AMQP_DURABLE);
$ex->setName($params['exchangeName']);
$ex->setType(AMQP_EX_TYPE_DIRECT);
$ex->declareExchange();

$queue=new AMQPQueue($channel);

$queue->setName($params['queueName']);
$queue->setFlags(AMQP_DURABLE);
$queue->setArguments([
    'x-dead-letter-exchange' => 'delay_exchange',
    'x-dead-letter-routing-key' => 'delay_route',
    'x-message-ttl' => 5000, //毫秒，如果队列test_cache_queue已经存在，除非删除，否者修改时间会报错
]);
$queue->declareQueue();

$queue->bind($params['exchangeName'],$params['routeKey']);

$msg='product_time:'.date("Y-m-d H:i:s",time());
$r=$ex->publish($msg,$params['routeKey'],AMQP_MANDATORY,['delivery_mode'=>2]);
if($r){
    echo "send _success".PHP_EOL;
}

/*
首先由正常队列(test_cache_queue)和正常exchange（test_cache_exchange），两者相绑定。

该正常队列设置了死信路由(delay_exchange)和死信路由key以及TTL,生产者生产消息到正常队列和正常路由上.

当正常队列设置TTL时间一到，那延迟消息就会自动发布到死信路由

消费者通过死信路由(delay_exchange)和死信队列(delay_queue)来消费

http://blog.51cto.com/chinalx1/2162676
https://www.cnblogs.com/haoxinyue/p/6613706.html

*/
















