<?php
include 'conn.php';


//创建连接和channel
$conn = new AMQPConnection($conn_args);
if (!$conn->connect()) {
    die("Cannot connect to the broker!\n");
}
$channel = new AMQPChannel($conn);



//创建交换机对象
$ex = new AMQPExchange($channel);
$ex->setName($e_name);

//发送消息
//$channel->startTransaction(); //开始事务
for($i=0; $i<5; ++$i){
    usleep(500);//休眠1秒
    //消息内容
    $message = "m$i!".date("Y-m-d H:i:s");
    echo "Send Message{$i}:".$ex->publish($message, $k_route)."\n";
}
//$channel->commitTransaction(); //提交事务

$conn->disconnect();