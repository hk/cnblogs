<?php

require_once "./../vendor/autoload.php";

use \alibaba\nacos\NacosConfig;
use \alibaba\nacos\Naming;

NacosConfig::setHost("http://192.168.0.100:8848/"); // 配置中心地址
$naming = Naming::init(
    "nacos.test.1",
    "11.11.11.11",
    "8848"
);

$r=$naming->register();
var_dump($r);