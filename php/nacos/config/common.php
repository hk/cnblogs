<?php

require_once "./../vendor/autoload.php";

use \alibaba\nacos\Nacos;
use \alibaba\nacos\NacosConfig;

$localCache = dirname(__FILE__) . '/nacos_cache/';
//默认  /nacos/config/dev_nacos/snapshot/LARAVEL
NacosConfig::setSnapshotPath($localCache);

/**
 * @return Nacos
 */
function getnacos(){
    return Nacos::init(
        "http://192.168.0.100:8848/",
        "public",
        "app",
        "DEFAULT_GROUP",
        ""
    );
}


