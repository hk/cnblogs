--5.1及其之前是 unpack,5.2移到table.unpack
--unpack它接受一个数组(table)作为参数,并默认从下标1开始返回数组的所有元素
local arr={
	1,3,5,'a',
	p="php"
}
u=table.unpack(arr)
print(table.unpack(arr)) --1       3       5       a
print(u,type(u))		--1       number

