print('math','--------------------------------')
for i in pairs(math) do
	print(i,math[i])
end
print('---------------------------------------')
--[[
math    --------------------------------
tan     function: 6299c260
fmod    function: 6299cc00
ult     function: 6299c740
max     function: 6299c600
floor   function: 6299caa0
mininteger      -9223372036854775808
min     function: 6299c570
sin     function: 6299c2f0
tointeger       function: 6299cb00
atan    function: 6299c820
cos     function: 6299c7e0
atan2   function: 6299c820
log10   function: 6299c040
modf    function: 6299c9e0
rad     function: 6299bfc0
pi      3.1415926535898
exp     function: 6299c7a0
sqrt    function: 6299c2a0
random  function: 6299c380
ceil    function: 6299c980
huge    inf
frexp   function: 6299c0e0
tanh    function: 6299c1a0
abs     function: 6299cb70
maxinteger      9223372036854775807
ldexp   function: 6299c080
pow     function: 6299c140
type    function: 6299c900
sinh    function: 6299c1e0
acos    function: 6299c8c0
log     function: 6299c690
asin    function: 6299c880
randomseed      function: 6299c330
deg     function: 6299c000
cosh    function: 6299c220
---------------------------------------


]]