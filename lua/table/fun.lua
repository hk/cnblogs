print('table','--------------------------------')
for i in pairs(table) do
	print(i,table[i])
end
print('---------------------------------------')

--数组转字符串
local arr={1,3,'abc',a="ajax","php"}
print(table.concat(arr))
print(table.concat(arr,'-'))
print(table.concat(arr,'-',2))
print(table.concat(arr,'-',2,3))

--向数组中插入元素 table.insert(arr,pos,value) 
--[[
向指定索引位置pos插入value ,并将后面的元素顺序后移，默认pos是数组的长度+1 即在尾部插入
]]

table.insert(arr,'cpp')
print(table.unpack(arr))
table.insert(arr,1,'java')
print(table.unpack(arr))
print('-----------------')
--[[
从数组中弹出一个元素
table.remove(arr,pos)
]]

table.remove(arr)
print(table.unpack(arr))
table.remove(arr,4)  --弹出索引为4的
print(table.unpack(arr))

--[[
table   --------------------------------
pack    function: 62995160
remove  function: 62995c40
sort    function: 62995870
concat  function: 62995fa0
insert  function: 62995e00
move    function: 62995940
unpack  function: 62994fc0
---------------------------------------
13abcphp
1-3-abc-php
3-abc-php
3-abc
1       3       abc     php     cpp
java    1       3       abc     php     cpp
-----------------
java    1       3       abc     php
java    1       3       php
]]






