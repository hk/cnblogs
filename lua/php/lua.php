<?php
require "./vendor/autoload.php";

class HMGetAll extends \Predis\Command\ScriptCommand{

    public function getKeysCount()
    {
        return false;
    }
    public function getScript(){
        return
        <<<LUA
        local result = {}
        for i ,v in ipairs(ARGV) do
            result[i] =redis.call('HGETALL',v)
        end
        return result
LUA;

    }
}
$client= new \Predis\Client(
    [
        'scheme'=>'tcp',
        'host' => '192.168.0.250',
        'port' => 6379,
        ]
);
//����hmgetall����
$client->getProfile()->defineCommand("hmgetall",'HMGetAll');

var_dump($client->hgetall('user:1'));
var_dump($client->hgetall('user:2'));
//ִ��hmgetall

$value=$client->hmgetall('user:1','user:2');
var_dump($value);
