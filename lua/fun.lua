local function square( ... )
	local argv={...}
	for i=1,#argv do
		argv[i]=argv[i]*argv[i]
	end
	return table.unpack(argv)
end
a,b,c=square(2,3,4)
print(a,b,c)  --4       9       16
-- return table.unpack(argv) 相当于 return argv[1],argv[2],argv[3]
