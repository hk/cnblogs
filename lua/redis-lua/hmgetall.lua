--获取指定的多个hash key

local result={}
for i,v in ipairs(KEYS) do
	result[i]=redis.call('HGETALL',v)
end
return result

--[[
redis-cli --eval hmgetall.lua user:1 user:2

[root@centos1 redis-lua]# redis-cli --eval hmgetall.lua user:1 user:2
1) 1) "name"
   2) "hk"
   3) "age"
   4) "20"
2) 1) "name"
   2) "hk2"
   3) "age"
   4) "22"

]]
