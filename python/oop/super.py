'''
调用父类
经典类:父类.方法
新式类:同上 /super(当前类,self).方法()

多继承属性搜索顺序不同

经典类: 不支持super
    先深入继承树左侧，再返回，开始找右侧
新式类:(python3 默认)
    先水平搜索，然后再向上移动
'''
'''
class A:
    def __init__(self):
        print("enter A")
        print("leave A")
class B(A):
    def __init__(self):
        print("inter B")
        A.__init__(self)
        print("leave B")
class C(A):
    def __init__(self):
        print("enter C")
        A.__init__(self)
        print("leave C")
class D(B,C):
    def __init__(self):
        print("enter D")
        B.__init__(self)
        C.__init__(self)
        print("leave D")

d=D()
'''
'''
enter D
inter B
enter A
leave A
leave B
enter C
enter A
leave A
leave C
leave D
'''

class A:
    def __init__(self):
        print("enter A")
        print("leave A")
class B(A):
    def __init__(self):
        print("enter B")
        super(B,self).__init__()
        print("leave B")
class C(A):
    def __init__(self):
        print("enter C")
        super(C,self).__init__()
        print("leave C")
class D(B,C):
    def __init__(self):
        print("enter D")
        super(D,self).__init__()
        print("leave D")

d=D()
























