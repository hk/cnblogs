#print("内置装饰器")

'''
三个内置装饰器

staticmethod:类静态方法
    与成员方法的区别是没有self参数,并且可以在类不进行实例化的情况下调用
    不加装饰器,类可访问，对象不可以，加上装饰器后，类和对象均可访问
classmethod:类方法
    与成员方法的区别在于所接收的第一个参数不是self(类实例的指针),而是cls(当前类的具体类型)
    让类也可直接调用方法
property:属性方法
        将一个类方法转变成一个类属性，只读属性

'''
import time
# 装饰器函数，适用于普通函数 和类方法(person里sleep)
def consume(fun):
    def warpper(self_):
        start_time=time.time()
        fun(self_)
        ent_time=time.time()
        print("consume %f"%(ent_time-start_time))
    return warpper
@consume
def work(arg=None):
    print("work start")
    time.sleep(3)
    print("work end")



class Person:
    name='Tom'
    gender='male'
    age=25
    __money=8000
    def __init__(self,name,age):
        self.name=name
        self.age=age
    @classmethod
    def say(self):
        print("my name is %s,i have %d"%(self.name,self.__money))
        return self.name
    def __spa(self):
        print("%s want a spa"%self.name)
    def __str__(self):
        return "persor"
    @staticmethod
    def bye():
        print("GAME OVER")
    @property
    def desc(self):
        return "%s %s"%(self.name,self.age)
    def getname(self):
        return self.name
    def setname(self,name):
        self.name=name
    name_age=property(getname,setname)

    @consume
    def sleep(self):
        print('start sleep')
        time.sleep(2)
        print('end sleep')

# 装饰器类修饰普通函数
class Decorator:
    def __init__(self,f):
        self.f=f
    def __call__(self, *args, **kwargs):
        print("decorator start")
        self.f()
        print("decorator end")
@Decorator
def func():
    print("func")


'''
p = Decorator(func) # p是类Decorator的一个实例
p() # 实现了__call__()方法后，p可以被调用
'''















