'''
多态
'''
class Triangle:
    def __init__(self,w,h):
        self.w=w
        self.h=h
    def getArea(self):
        area=self.w*self.h/2
        return area
class Square:
    def __init__(self,size):
        self.size=size
    def getArea(self):
        area=self.size*self.size
        return area

'''
继承,重载方法，重载运算符
'''

class father:
    money=100000
    __money__=50000000
    car="奔驰"

    def driver(self):
        print("i can driver %s"%self.car)
class mother:
    money=500

class son(father,mother):
    pass
    car ="飞机"
    def pay(self):
        print(self.money,self.__money__) #100000 50000000
    def driver(self):
        father.driver(self)
        print("I can driver %s"%self.car)

tom=father()
print(tom.money)
tom.driver()

print('#'*20)

john=son()
john.driver()
john.pay()
