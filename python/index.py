from oop import decorator
p=decorator.Person("hk",18)
import fun

# decorator.Person.say() #报错
# decorator.Person.bye() #加装饰器前 可直接调用
#p.bye() #报错
# print('-'*50)
# 加上装饰器后
# decorator.Person.bye()
# p.bye()
#加上classmethod 后，均可调用
# p.say()
# decorator.Person.say()

# print(p.desc)
# print(p.getname())
# p.setname("hkui2010")
# print(p.getname())
# print('.'*50)
# print(p.name_age)
#----------------------

#异常
# import exception
# print(exception.name)

# decorator.work(None)
#
# p.sleep()
# decorator.func()

from decorator import fun
# from decorator import obj
#from decorator import obj1
#example=obj1.example()
# example.myfunc()
# print('*'*20)
#example.myfunc2(1,2)

obj={"a":1,'bb':22}

arg=(k for k in obj)
print(arg,type(arg))

for i in arg:
    print(i)


t="|".join("%s"% k for k in obj)

print(t)


_keys = ", ".join('%s' % arg)

print(_keys)























