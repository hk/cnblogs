#yield与return
#菲波那切数列
def fab(max):
    n,a,b=0,0,1
    while n<max:
        print(b)
        a,b=b,a+b
        n+=1
# max=int(input("a num:\n"))
# fab(max)

def f():
    print("one")
    yield 1
    print("two")
    yield 2
    print("three")
    yield 3
# r=f()
# print(r)
# print(r.__next__())
# print(r.__next__())
# print(r.__next__())
#yield方式的fab
def yield_fab(max):
    n,a,b=0,0,1
    while n<max:
        yield b
        a,b=b,a+b
        n+=1
max=int(input("a num :\n"))


for i in yield_fab(max):
    print(i)







