#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2018-08-05 21:19:50
# Project: mlepu

from pyspider.libs.base_handler import *


class Handler(BaseHandler):
    crawl_config = {
        'validate_cert':False
    }

    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl('http://m.lepu.cn/beijing/shop', callback=self.index_page)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        for each in response.doc('a[href^="http"]').items():
            self.crawl(each.attr.href, callback=self.detail_page)

    @config(priority=2)
    def detail_page(self, response):
        return {
            "url": response.url,
            "title": response.doc('title').text(),
        }

