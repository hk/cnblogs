'''
导入模块
import 模块名称
import 模块名称 as 别名
from 模块名 import 函数名/类名/*

'''

# from mymodule.mod1 import show1
# show1()
# def show1():
#     print("show1")
# show1()




# import  mymodule.mod1 as mod1
# mod1.show()
#
# print(dir(mod1))

'''
模块优化 生成模块缓存
*.pyc
python -m py_compile file.py
脚本
import py_compile
py_compile.compile("mod1.py","module.pyc")

*.pyo或opt

python -O -m py_compile module.py
python -OO -m py_compile module.py 依赖关系可能有问题
'''
#寻找模块路径
# import sys
# for i in sys.path:
#     print(i)
'''
import os
import sys

mymodule_path=os.getcwd()+"/module/mymodule"
sys.path.append(mymodule_path)


import mod1
mod1.show1()
'''

'''
模块太多，包管理
包:目录/文件夹 ,必须包含__init__.py

A   /__init__
    /B          /__init__
                module.py
    /C           __init__
                module.py
    /module.py
'''
import os
import sys

mymodule_path=os.getcwd()+"/module/A"
# sys.path.append(mymodule_path)

from A import amod1
amod1.fun()
from A.B import bmod1
bmod1.fun()








