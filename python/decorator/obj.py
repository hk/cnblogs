#1.装饰器参数是一个类
name="obj"
class locker:
    def __init__(self):
        print(__class__.__name__,"init")
    @staticmethod
    def acquire():
        print(__class__.__name__, "acquire")
    @staticmethod
    def release():
        print(__class__.__name__, "release")
class cls(locker):
    pass


def deco(cls):
    def _deco(func):
        def __deco():
            print("before %s called [%s]"%(func.__name__,cls))
            cls.acquire()
            try:
                return func()
            finally:
                cls.release()
        return __deco
    return _deco
@deco(locker)
def myfunc():
    print(" myfunc called")
# myfunc()



#2.装饰器带类参数 ，并分拆到其它py文件中国，一个函数对应多个装饰器

class mylocker:
    def __init__(self):
        print("mylocker.__init.__")
    @staticmethod
    def acquire():
        print("mylocker.acquire")
    @staticmethod
    def unlock():
        print("mylocker.unlock")

class lockerex(mylocker):
    @staticmethod
    def acquire():
        print("lockerex.acquire")

    @staticmethod
    def unlock():
        print("lockerex.unlock")

def lockhelper(cls):
    def _deco(func):
        def __deco(*args):
            print("before %s called"%func.__name__,func)
            cls.acquire()
            try:
                return func(*args)
            finally:
                cls.unlock()
        return __deco
    return _deco

