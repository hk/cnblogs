
if __name__ == "__main__":
    import obj as myobj
else:
    from decorator import  obj as myobj

name="%s obj1"%(myobj.name)



class example:
    @myobj.lockhelper(myobj.mylocker)
    def myfunc(self):
        print("myfunc called")

    @myobj.lockhelper(myobj.mylocker)
    @myobj.lockhelper(myobj.lockerex)
    def myfunc2(self,a,b):
        print("myfunc2 called")
        return a+b



