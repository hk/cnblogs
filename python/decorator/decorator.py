import  time

def deco(fun):
    def wrapper():
        startT=time.time()
        fun()
        endT=time.time()
        msecs=(endT-startT)*1000
        print("consume %f ms"%msecs)
    return wrapper


@deco    #使用@ 语法糖精简装饰器的代码

def func():
    print("func begin")
    time.sleep(2)
    print("func end")

#func=deco(func)  #类似@deco

func()




