import time
#1.不带参数的装饰器装饰 带参数和不带参数的函数
def consume(func):
    def _consume(*arg):
        start_time=time.time()
        ret=func(*arg)
        ent_time=time.time()
        return [ret,ent_time-start_time]
    return _consume
@consume
def daywork():
    return 8
# t=daywork()
# print(t)
@consume
def workday(week):
    wt=0
    if week >=1 and week <=5:
        wt=8
    elif week == 6:
        wt=6
    else:
        wt=0
    time.sleep(wt/10)
    return wt
# wt=workday(7)
# print(wt)




#3.带参数的装饰器装饰带参数的函数
# 第一次层 装饰器参数，第二层函数名做为参数，第三层函数的参数
def deco(arg):
    def _deco(func):
        def __deco(*args):
            print("before %s called [%s]"%(func.__name__,arg))
            func(*args)
            print("after %s called [%s]" % (func.__name__, arg))
        return __deco
    return _deco
@deco(2)
def myfunc(a,b):
    print("myfunc called",a,b)
# myfunc(1,2)
