def f(step=1):
    global a
    a+=step

def buy(money,product):
    print("%d元可以买%s"%(money,product))

# buy(product="安全套",money=50)
listarg=[5,"巧克力"]
# list参数
# buy(*listarg)
dictarg={"money":100000,'product':"房子"}
# print(*dictarg) #money product
# print(**dictarg); 语法错误

# buy(**dictarg) #100000元可以买房子

'''
多参数冗余处理
传值时必须统一,要么全不指定键要么全部指定
'''
def buy1(money,pro,*arg):
    print("%d元可以买%s"%(money,pro))
    print("arg:",arg)

# buy1(1,"花生",'a','b','c')

def buy2(money,pro,*arg,**dst):
    print("%d元可买%s"%(money,pro))
    print("arg:",arg);
    print("dst:",dst)
# buy2(pro="煎饼",money=6,z="11",k=12)
'''
6元可买煎饼
arg: ()
dst: {'z': '11', 'k': 12}
'''
'''
匿名函数lambda表达式
'''
def f(x,y):
    return x+y

from functools import reduce
# print(reduce(f,[1,2,3,4,5]))#15
# print("------reduce------")
# print(reduce(lambda x,y:x+y,[1,2,3,4]))#10

foo=[0,1,2,3,4,5,6,7,8,9]
# print("###filter#####")
r=filter(lambda x:x%3==0,foo)
# print(r) #filter object
# for i in r:
#     print(i)
# print("---------------------")
# m=map(lambda x:x*2+1,foo)
# for i in m:
#     print(i)

def func(*arg):
    print(arg,type(arg))
def func1(**arg):
    print(arg,type(arg))
# func(1,2,3)

# func1(a=1,b=2)

def func2(x,*arg,**karg):
    print(x)
    print("-"*20)
    print(arg)
    print("-" * 20)
    print(karg)
# func2(1,2,3,a=4)





















