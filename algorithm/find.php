<?php
//二分查找，必须是有序的
$arr=[1,3,5,7,8,10,15,20];

function find($arr,$num){
    $left=0;
    $right=count($arr)-1;
    while($left<=$right){
        $mid_index=intval(($left+$right)/2);
        if($num<$arr[$mid_index]){
            $right=$mid_index-1;
        }elseif($num>$arr[$mid_index]){
            $left=$mid_index+1;
        }else{
            return $arr[$mid_index];
        }
    }
}
echo find($arr,2);