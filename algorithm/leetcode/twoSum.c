/**
给定 nums = [2, 7, 11, 15], target = 9

因为 nums[0] + nums[1] = 2 + 7 = 9
所以返回 [0, 1]
*/
#include <stdio.h>
#include <stdlib.h>

int  *twoSum(int* nums, int numsSize, int target) {
    int i=0,j=0;
    int *ret=malloc(2*sizeof(int));
    for(;i<numsSize-1;i++){
    	for(j=i+1;j<numsSize;j++){
    		if(nums[i]+nums[j] == target){
    			*ret=i;
    			ret++;
    			*ret=j;
    			// ret=&i;
    			// ret++;
    			// ret=&j;
    			return --ret;
    		}
    	}
    }
    return ret;
}


int main(int argc,char *argv[],char *environment){
    int arr[]={3,4,5,7,11,21};
    int *ret;
    ret=twoSum(arr,6,12);
    printf("%d\n",*(ret) );
   	printf("%d\n",*(ret+1) );
   
    return 0;
}