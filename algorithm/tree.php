<?php
class Node{
    private $parent;
    private $left;
    private $right;

    private $data;
    public function __construct($data){
        $this->data=$data;
    }

    public function setLeft(Node $node){
        $node->parent=$this;
        $this->left=$node;
        return $node;
    }
    public function setRight(Node $node){
        $node->parent=$this;
        $this->right=$node;
        return $node;
    }
    //前序遍历
    public function preOrder($node){
        if($node instanceof Node){
            echo $node->data;
            $this->preOrder($node->left);
            $this->preOrder($node->right);
        }
    }
    //中序遍历
    public function midOrder($node){
        if($node instanceof Node){
            $this->midOrder($node->left);
            echo $node->data;
            $this->midOrder($node->right);
        }
    }
    //后序遍历
    public function sufOrder($node){
        if($node instanceof Node){
            $this->sufOrder($node->left);
            $this->sufOrder($node->right);
            echo $node->data;
        }
    }

}
$tree=new Node(1);
$left=$tree->setLeft(new Node(2));
$right=$tree->setRight(new Node(3));

$left->setLeft(new Node(4));
$left->setRight(new Node(5));

$right->setLeft(new Node(6));
//print_r($tree);

$tree->preOrder($tree);
echo "<hr>";
$tree->midOrder($tree);
echo "<hr>";
$tree->sufOrder($tree);


/*
 * 二叉树的遍历
 * 1.前序遍历
 * 2.中序遍历
 * 3。后续遍历
 *
*/

