<?php
//选择排序
function p($arr){
    return join(',',$arr)."<br>";
}
$arr=[3,1,2,7,5,4,9,0,7,-1,6];
function selectSort($arr){
    $len=count($arr);
    for($i=0;$i<$len-1;$i++){
        $min_index=$i;
        for($j=$i+1;$j<$len;$j++){
            if($arr[$j]<$arr[$min_index]){
                $min_index=$j;
            }
        }
        if($min_index!=$i){
            $tmp=$arr[$i];
            $arr[$i]=$arr[$min_index];
            $arr[$min_index]=$tmp;
        }
    }
    return $arr;
}
echo p($arr);
echo "<hr>";
echo p(selectSort($arr));