<?php
//插入排序
function p($arr){
    return join(',',$arr)."<br>";
}
$arr=[5,4,2,3,1];
echo p($arr);
echo "<hr>";

function insertSort($arr){
    $len=count($arr);
    for($i=1;$i<$len;$i++){
        $j=$i;
        $tmp=$arr[$i];
        while($j>0 && $arr[$j-1]>$tmp){
            $arr[$j]=$arr[$j-1];
            $j--;
        }
        $arr[$j]=$tmp;
    }
    return $arr;
}
echo p(insertSort($arr));

/**
 * 5,4,2,3,1
 * 4,5,2,3,1
 * 2,4,5,3,1
 * 2,3,4,5,1
 * 1,2,3,4,5
 */


