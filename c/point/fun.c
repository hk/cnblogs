#include <stdio.h>
int sum(int a,int b){
	return a+b;
}
typedef int(*PFUN)(int);
//返回函数指针
PFUN fun(int a){
	return NULL;
}

int main(){
	printf("%p\n",sum );

	int (*pSum)(int,int);
	pSum=sum ;

	//定义函数指针类型 
	typedef int(*PSUM)(int,int);

	PSUM pSum2=sum;
	//通过指针变量调用函数
	int c=pSum2(1,2);
	printf("c=%d\n",c );
	// p是一个函数指针,指向的函数、参数整型
	//返回值又是一个函数指针(参数，返回值都是整型)
	int (* (*p)(int))(int);
	p=fun;
	p(123);

	return 0;
}
/**
函数指针变量定义

返回类型 （* 变量名)(参数1，参数2 ....)

int sum(int a,int b){}

int (*pSum)(int a,int b) ; //函数指针变量

pSum=sum;  //给指针赋地址值

优先级
int (* (*p)(int))(int);
() 的优先级最高
p先与*结合,说明p首先是个指针A
再与后面()结合,说明A指向的内容是一个函数A,
再与括号里的Int结合,说明该函数参数是一个Int类型
再与(*p)前的* 结合说明该函数返回值是一个指针B,
再与后面的()结合，说明该指针B是函数指针



*/