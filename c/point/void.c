#include <stdio.h>

int main(){
	int a=10;
	char b='A';
	float c=12.345;

	//空类型可以指向任何类型的变量
	void *pa=&a;
	void *pb=&b;
	void *pc=&c;

	//使用原来的变量

	// int a1=*pa;  错误
	// int a1=(int)(*pa);  错误
	//强制转换指针
	int a1=  *((int *)pa) ;
	int b1= *(char *)pb;
	printf("a1=%d\n",a1 );
	printf("b1=%c\n",b1 );
	/*
   空指针
void * 可以指向任何类型的地址

野指针:指向一个非法的或已销毁的内存的指针
危害:对系统造成不可预知的危害

为什么会出现野指针
1.指针变量没有被初始化,它的缺省值是随机的,它会乱指一气
char *p; //野指针  p的值是随机的

tips:指针变量再创建的同时应当被初始化,要么将其设置为NULL,要么让它指向合法的内存
例如
char *p=NULL ;
char *p=(char *) malloc(100);

2.指针p被feee或者delete之后,只是把指针所指的内存给释放掉,没有改变指针的值。此时p沦为野指针






	*/
}