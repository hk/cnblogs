#include <stdio.h>
#include <stdlib.h>

int main(){
	int a; //随机
	printf("a=%d\n",a );
	int *pa; //野指针 部分编译器有优化为nil
	printf("pa=%p\n",pa);

	int *pb=NULL;
	printf("pb=%p\n",pb );

	int *pc=NULL; //((void *)0)
	pc=(int *)malloc(4); //合法

	printf("pc=%p\n",pc );

	free(pc);
	printf("pc=%p\n",pc );
	pc=NULL;
}