#include <stdio.h>

void swap(int *a,int *b){
	int tmp=*a;
	*a=*b;
	*b=tmp;
}
void main(){
	int a=1;int b=2;
	swap(&a,&b);
	printf("a=%d,b=%d\n",a,b );

	int c=3;int d=4;
	int *pc=&c;
	int *pd=&d;
	swap(pc,pd);
	printf("%d,%d\n",*pc,*pd );
}