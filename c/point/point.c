/**
int ** 整型指针的指针类型
char **
float **
*/
#include <stdio.h>

void main(){
	int a=1;
	int b=2;
	int c=3;
	//指针数组,本身数组名是一个地址，数组元素又是一个地址，双重指针
	int *d[3]={&a,&b,&c};

	int **p=d; //指针的指针就是为指针数组而生的
	int i=0;
	for(;i<3;i++){
		printf("%d\n",*(p[i]) );
	}
	printf("----------------------\n");
	for(i=0;i<3;i++){
		printf("%d\n", *(*(p+i)));
	}
}