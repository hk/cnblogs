#include <stdio.h>
#include <stdlib.h>

int *func(void){
	int *ret;
	int a=10;
	int b=100;
	printf("1:a=%p,b=%p,ret=%p\n",&a,&b,ret);
	ret=&a;
	ret++;
	ret=&b;
	printf("2:a=%p,b=%p,ret=%p\n",&a,&b,ret);
	
	ret++;
	printf("3:a=%p,b=%p,ret=%p\n",&a,&b,ret);
	return ret;
}

int main(int argc,char *argv[]){
	int *ret=func();
	printf("ret=%p\n", ret);
	printf("ret=%d\n",*ret );
	printf("ret+1=%d\n",*(ret-1) );
	return 0;

}