#include <stdio.h>

int main(){
	int const *p ; 
	const int a=1;
	int b=2;
	printf("%p=%d\n",&a,a);
	printf("%p=%d\n",&b,b);
	p=&a; //必须是常量指针才行
	printf("%p--->%d\n",p,*p );
	return 0;
}