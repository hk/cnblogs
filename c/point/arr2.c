#include <stdio.h>

int main(){
	int a[2][3]={{6,5,4},{1,2,3}};
	/*
	a     		行指针
	a[0]
	&a[0]  		行指针
	&a[0][0]
	*/
	int (*p1)[3]=a;
	int *p2=a[0];
	int (*p3)[3]=&a[0];
	int *p4=&a[0][0];
	printf("%p,%p\n",p1,p1+1 );
	printf("%p,%p\n",p2,p2+1 );
	printf("%p,%p\n",p3,p3+1 );
	printf("%p,%p\n",p4,p4+1 );
	/*
		0x7ffd07c91550,0x7ffd07c9155c
		0x7ffd07c91550,0x7ffd07c91554
		0x7ffd07c91550,0x7ffd07c9155c
		0x7ffd07c91550,0x7ffd07c91554
	*/
	int i,j;
	for(i=0;i<2;i++){
		for(j=0;j<3;j++){
			printf("p1[%d][%d]=%d\n",i,j,p1[i][j] );
		}
	}
	
	for(i=0;i<2;i++){
		for(j=0;j<3;j++){
			printf("%d\t", *(p1[i]+j));//p1[i] 一维数组首地址
		}
	}
	printf("\n");

	for(i=0;i<2;i++){
		for(j=0;j<3;j++){
			printf("%d\t", *(*(p1+i)+j));//p1是行指针 *(p1+i)等价于p1[i]
		}
	}
	printf("\n");
	for(i=0;i<2;i++){
		for(j=0;j<3;j++){
			printf("%d\t", *(*(&p1[i])+j));//p1是行指针 *(&p1[i]) 等价于p1[i]
		}
	}
	printf("\n");

	


	return 0;
}
/**
a[M][N]
行指针:		 a   		&a[N]   int (*p)[N]
普通指针:	&a[M][N]	a[N]	int *

*/