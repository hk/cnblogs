#include <stdio.h>

void main(){
	//字符串常量就是一个地址,相当于一个无名数组
	printf("%p\n","php");
	//把首地址赋给指针变量p,相当于有名字了
	char *p="php";
	char *pp=p;
	int i=0;

	//指针变量相当于数组名用
	for(;i<4;i++){
		printf("%c\n",p[i]);
	}
	printf("--------------------------\n");

	for(i=0;i<4;i++){
		printf("%c\n",*(p++));
	}

	printf("--------------------------\n");
	p=pp;
	for(i=0;i<4;i++){
		printf("%c\n",*(p+i));
	}
	//字符串常量不可改变
	//p[0]='a'; coredump

}
